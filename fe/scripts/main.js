require(['axios.min'], function(axios) { // https://github.com/axios/axios
    require(['d3.min'], function(d3) { // http://bl.ocks.org/ndarville/7075823 http://christopheviau.com/d3_tutorial/
        console.log(d3)
        async function loadData() {
            const resp = await axios.get('http://localhost:3000/getData');
            console.log(resp.data);
            
            const parsedCSV = d3.csvParseRows(resp.data);
            d3.select("body")
                .append("table")
                .selectAll("tr")
                .data(parsedCSV).enter()
                .append("tr")
                .selectAll("td")
                    .data(function(d) { return d; }).enter()
                    .append("td")
                    .text(function(d) { return d; });
        }
        
        loadData();        
    });
});
