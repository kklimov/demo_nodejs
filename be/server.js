const fs = require('fs')
const express = require('express') // https://expressjs.com/en/starter/hello-world.html
const cors = require('cors'); // https://medium.com/@alexishevia/using-cors-in-express-cac7e29b005b
const shell = require('shelljs') // https://www.npmjs.com/package/shelljs

const app = express()
// allow cross origin requests by default but better restrict to certain domain
app.use(cors());

// your be api handler here
app.get('/getData', (req, res) => {
    shell.exec('./createcsv.sh')
    const content = fs.readFileSync('./data.csv', 'utf8')
    res.send(content)
})

app.listen(3000, () => console.log('Server is running on localhost:3000'))